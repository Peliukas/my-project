import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

let store = new Vuex.Store({
  state: {
    links: [],
    newLink: ''
  },
  mutations: {
    GET_LINK (state, link) {
      state.newLink = link
      commitLinksToStorage(state.links)
    },
    ADD_LINK (state) {
      state.links.push({
        body: state.newLink
      })
      commitLinksToStorage(state.links)
    },
    SET_LINKS (state, links) {
      state.links = links
    }
  },
  actions: {
    getLink ({commit}, link) {
      commit('GET_LINK', link)
    },
    addLink ({commit}) {
      commit('ADD_LINK')
    },
    initLinks ({commit}, links) {
      commit('SET_LINKS', links)
    }
  },
  getters: {
    newLink: state => state.newLink,
    links: state => state.links
  }

})

function commitLinksToStorage (links) {
  localStorage.setItem('links', JSON.stringify(links))
}

function linksFromStorage () {
  let linksJSON = localStorage.getItem('links')
  console.log(linksJSON)
  return JSON.parse(linksJSON) || []
}

store.dispatch('initLinks', linksFromStorage())
export default store
