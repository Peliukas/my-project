# my-project

> Just a project for fun

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# for this app to work, You must install a plugin to the browser to enable 
cross-origin resources sharing in the localhost
For example: 
https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi

# Have fun :)
